//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=SimpleAddress.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 06/07/2020 10:01
//  Altered - 06/07/2020 12:45 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Library.Interfaces;

namespace Library
    {
        /// <summary>
        /// </summary>
        public sealed class SimpleAddress : AddressBase, ISimpleAddress, IEquatable<SimpleAddress>
            {
                /// <inheritdoc />
                public string PrimaryAddress { get; set; } = "";

                /// <inheritdoc />
                public string SecondaryAddress { get; set; } = "";

                /// <inheritdoc />
                public string Street { get; set; } = "";

                /// <inheritdoc />
                public string Location { get; set; } = "";

                /// <inheritdoc />
                public string Town { get; set; } = "";

                /// <inheritdoc />
                public string County { get; set; } = "";

                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator !=(SimpleAddress lhs, SimpleAddress rhs) => !(lhs == rhs);

                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator ==(SimpleAddress lhs, SimpleAddress rhs)
                    {
                        // Check for null on left side.
                        if (lhs is null)
                            {
                                return rhs is null;

                                // Only the left side is null.
                            }

                        // Equals handles case of null on right side.
                        return lhs.Equals(rhs);
                    }


                /// <inheritdoc />
                public override bool IsEmptyAddress() => FullAddress() == "";

                /// <inheritdoc />
                public override string FullAddress() =>
                    base.FullAddress(PrimaryAddress, SecondaryAddress, Street, Location, Town, County);

                /// <summary>
                /// </summary>
                /// <param name="obj"></param>
                /// <returns></returns>
                public override bool Equals(object obj) => Equals(obj as SimpleAddress);

                /// <summary>
                /// </summary>
                /// <param name="other"></param>
                /// <returns></returns>
                public bool Equals(SimpleAddress other) =>
                    other != null &&
                    base.Equals(other) &&
                    EqualityComparer<IUprn>.Default.Equals(Uprn, other.Uprn) &&
                    EqualityComparer<IUsrn>.Default.Equals(Usrn, other.Usrn) &&
                    EqualityComparer<IPostCode>.Default.Equals(PostCode, other.PostCode);

                /// <summary>
                /// </summary>
                /// <returns></returns>
                public override int GetHashCode()
                    {
                        var hashCode = -2017556967;
                        hashCode = (hashCode * -1521134295) + base.GetHashCode();
                        hashCode = (hashCode * -1521134295) + EqualityComparer<IUprn>.Default.GetHashCode(Uprn);
                        hashCode = (hashCode * -1521134295) + EqualityComparer<IUsrn>.Default.GetHashCode(Usrn);
                        hashCode = (hashCode * -1521134295) + EqualityComparer<IPostCode>.Default.GetHashCode(PostCode);
                        return hashCode;
                    }
            }
    }