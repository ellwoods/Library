//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=SimpleAddressLine.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 12:46 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Library.Interfaces;

namespace Library
    {
        /// <summary>
        /// </summary>
        public sealed class SimpleAddressLine : AddressBase, ISimpleAddressLine, IEquatable<SimpleAddressLine>
            {
                /// <inheritdoc />
                public override bool IsEmptyAddress() => FullAddress().Trim() == "";

                /// <inheritdoc />
                public override string FullAddress() =>
                    base.FullAddress(Address1, Address2, Address3, Address4, Address5, "");

                /// <inheritdoc />
                public string Address1 { get; set; } = "";

                /// <inheritdoc />
                public string Address2 { get; set; } = "";

                /// <inheritdoc />
                public string Address3 { get; set; } = "";

                /// <inheritdoc />
                public string Address4 { get; set; } = "";

                /// <inheritdoc />
                public string Address5 { get; set; } = "";


                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator ==(SimpleAddressLine lhs, SimpleAddressLine rhs)
                    {
                        // Check for null on left side.
                        if (lhs is null)
                            {
                                return rhs is null;

                                // Only the left side is null.
                            }

                        // Equals handles case of null on right side.
                        return lhs.Equals(rhs);
                    }

                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator !=(SimpleAddressLine lhs, SimpleAddressLine rhs) => !(lhs == rhs);

                /// <summary>
                /// </summary>
                /// <param name="obj"></param>
                /// <returns></returns>
                public override bool Equals(object obj) => Equals(obj as SimpleAddressLine);

                /// <summary>
                /// </summary>
                /// <param name="other"></param>
                /// <returns></returns>
                public bool Equals(SimpleAddressLine other) =>
                    other != null &&
                    base.Equals(other) &&
                    EqualityComparer<IUprn>.Default.Equals(Uprn, other.Uprn) &&
                    EqualityComparer<IUsrn>.Default.Equals(Usrn, other.Usrn) &&
                    EqualityComparer<IPostCode>.Default.Equals(PostCode, other.PostCode);

                /// <summary>
                /// </summary>
                /// <returns></returns>
                public override int GetHashCode()
                    {
                        var hashCode = -2017556967;
                        hashCode = (hashCode * -1521134295) + base.GetHashCode();
                        hashCode = (hashCode * -1521134295) + EqualityComparer<IUprn>.Default.GetHashCode(Uprn);
                        hashCode = (hashCode * -1521134295) + EqualityComparer<IUsrn>.Default.GetHashCode(Usrn);
                        hashCode = (hashCode * -1521134295) + EqualityComparer<IPostCode>.Default.GetHashCode(PostCode);
                        return hashCode;
                    }
            }
    }