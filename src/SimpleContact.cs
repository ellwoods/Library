//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=SimpleContact.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 06/07/2020 10:01
//  Altered - 06/07/2020 12:48 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using Library.Interfaces;

namespace Library
    {
        /// <summary>
        /// </summary>
        public class SimpleContact : ISimpleContact
            {
                public SimpleContact() => Setup(new SimpleAddress());

                public SimpleContact(ISimpleAddress address) => Setup((IAddress)address);

                public SimpleContact(ISimpleAddressLine address) => Setup(address);

                public SimpleContact(ISimpleAddressNameNumber address) => Setup((IAddress)address);

                private void Setup(IAddress address)
                    {
                        Person = new SimplePerson();
                        Address = address;
                        PreferredPhone = new SimpleTelephoneNumber();
                        HomePhone = new SimpleTelephoneNumber();
                        MobilePhone = new SimpleTelephoneNumber();
                        WorkPhone = new SimpleTelephoneNumber();
                    }

                #region Implementation of ISimpleContact

                /// <inheritdoc />
                public IPerson Person { get; set; }

                /// <inheritdoc />
                public IAddress Address { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber PreferredPhone { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber HomePhone { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber MobilePhone { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber WorkPhone { get; set; }

                /// <inheritdoc />
                public string emailAddress { get; set; }

                /// <inheritdoc />
                public string Notes { get; set; }

                #endregion
            }
    }