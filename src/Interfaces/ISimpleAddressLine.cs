//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=ISimpleAddressLine.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 12:26 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace Library.Interfaces
    {
        /// <summary>
        /// </summary>
        public interface ISimpleAddressLine : IAddress
            {
                /// <summary>
                /// </summary>
                string Address1 { get; set; }

                /// <summary>
                /// </summary>
                string Address2 { get; set; }

                /// <summary>
                /// </summary>
                string Address3 { get; set; }

                /// <summary>
                /// </summary>
                string Address4 { get; set; }

                /// <summary>
                /// </summary>
                string Address5 { get; set; }
            }
    }