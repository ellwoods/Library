//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IAddress.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 12:24 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;

namespace Library.Interfaces
    {
        /// <summary>
        /// </summary>
        public interface IAddress : IEquatable<IAddress>
            {
                /// <summary>
                /// </summary>
                IUprn Uprn { get; set; }

                /// <summary>
                /// </summary>
                IUsrn Usrn { get; set; }

                /// <summary>
                /// </summary>
                IPostCode PostCode { get; set; }

                /// <summary>
                /// </summary>
                double? Easting { get; set; }

                /// <summary>
                /// </summary>
                double? Northing { get; set; }

                /// <summary>
                ///     This is part of the address that can be used for sorting
                /// </summary>
                string AddressSortField { get; set; }

                /// <summary>
                ///     Is Empty Address
                /// </summary>
                /// <remarks>checks if the address lines are all empty</remarks>
                bool IsEmptyAddress();

                /// <summary>
                ///     Formatted Address
                /// </summary>
                /// <remarks>This is the address on a single line</remarks>
                string FullAddress(string line1, string line2, string line3, string line4,
                    string line5, string line6);

                /// <summary>
                ///     Formatted Address
                /// </summary>
                /// <returns></returns>
                /// <remarks>This is to be overridden, it's here so it's accessible</remarks>
                string FullAddress();
            }
    }