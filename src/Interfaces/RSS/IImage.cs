﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IImage.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:36 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Image interface.
        /// </summary>
        public interface IImage
            {
                /// <summary>
                ///     Gets or sets the description.
                /// </summary>
                /// <remarks>Any text here will be included in the Title attribute of the image in the HTML rendering</remarks>
                string Description { get; set; }

                // default 31 max 400

                /// <summary>
                ///     Gets or sets the pixel height.
                /// </summary>
                int? Height { get; set; }

                /// <summary>
                ///     Gets or sets the link.
                /// </summary>
                Uri Link { get; set; }

                /// <summary>
                ///     Gets or sets the title.
                /// </summary>
                string Title { get; set; }

                /// <summary>
                ///     Gets or sets the url.
                /// </summary>
                /// <remarks>url of Gif, Jpeg or Png image</remarks>
                Uri Url { get; set; }

                // default 88 max 144

                /// <summary>
                ///     Gets or sets the pixel width.
                /// </summary>
                int? Width { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }