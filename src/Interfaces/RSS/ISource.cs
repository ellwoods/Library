//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=ISource.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:37 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Source interface.
        /// </summary>
        /// <remarks>
        ///     optional
        ///     It's value is the name of the rss channel that the item came from derived from title
        /// </remarks>
        public interface ISource
            {
                // the value is the name of the rss channel the item came from

                /// <summary>
                ///     Gets or sets the url.
                /// </summary>
                Uri Url { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }