//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IItem.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:37 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Item interface.
        /// </summary>
        /// <remarks>All items are optional however at least one of title or description must be present</remarks>
        public interface IItem
            {
                // email

                /// <summary>
                ///     Gets or sets the author.
                /// </summary>
                string Author { get; set; }

                /// <summary>
                ///     Gets or sets the category.
                /// </summary>
                IEnumerable <ICategory> Category { get; set; }

                // url

                /// <summary>
                ///     Gets or sets the comments.
                /// </summary>
                string Comments { get; set; }

                /// <summary>
                ///     Gets or sets the description.
                /// </summary>
                /// <remarks>At least Description or <see cref="Title" /> must be present</remarks>
                string Description { get; set; }

                /// <summary>
                ///     Gets or sets the enclosure.
                /// </summary>
                IEnclosure Enclosure { get; set; }

                /// <summary>
                ///     Gets or sets the guid.
                /// </summary>
                IGuid Guid { get; set; }

                /// <summary>
                ///     Gets or sets the link.
                /// </summary>
                Uri Link { get; set; }

                // must be in the future

                /// <summary>
                ///     Gets or sets the pub date.
                /// </summary>
                DateTime PubDate { get; set; }

                /// <summary>
                ///     Gets or sets the source.
                /// </summary>
                ISource Source { get; set; }

                /// <summary>
                ///     Gets or sets the title.
                /// </summary>
                /// <remarks>At least title or <see cref="Description" /> must be present</remarks>
                string Title { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }