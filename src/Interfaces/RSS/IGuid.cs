//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IGuid.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:36 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System.Xml.Serialization;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Guid interface.
        /// </summary>
        public interface IGuid
            {
                /// <summary>
                ///     Gets or sets a value indicating whether is permalink.
                /// </summary>
                /// <remarks> default is true but this is optional</remarks>
                [XmlAttribute]
                bool IsPermalink { get; set; }

                /// <summary>
                ///     Gets or sets the value.
                /// </summary>
                string Value { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }