//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IEnclosure.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:36 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using System.Xml.Serialization;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Enclosure interface.
        /// </summary>
        public interface IEnclosure
            {
                /// <summary>
                ///     Gets or sets the length.
                /// </summary>
                /// <remarks>size in bytes</remarks>
                [XmlAttribute]
                long Length { get; set; }

                // MIME type

                /// <summary>
                ///     Gets or sets the MIME type.
                /// </summary>
                [XmlAttribute]
                string Type { get; set; }

                /// <summary>
                ///     Gets or sets the url.
                /// </summary>
                [XmlAttribute]
                Uri Url { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }