﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IChannel.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:36 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Channel interface.
        /// </summary>
        public interface IChannel
            {
                /// <summary>
                ///     Gets or sets the category.
                /// </summary>
                IEnumerable <ICategory> Category { get; set; }

                /// <summary>
                ///     Gets or sets the cloud.
                /// </summary>
                /// <remarks>
                ///     Allows processes to register with a cloud to be notified of updates to the channel
                ///     This feature is not implemented here
                /// </remarks>
                string Cloud { get; }

                /// <summary>
                ///     Gets or sets the copyright.
                /// </summary>
                string Copyright { get; set; }

                /// <summary>
                ///     Gets or sets the description.
                /// </summary>
                string Description { get; set; }

                /// <summary>
                ///     Gets or sets the docs.
                /// </summary>
                /// <remarks>The URL for the documentation for the format for this feed</remarks>
                Uri Docs { get; set; }

                /// <summary>
                ///     Gets or sets the generator.
                /// </summary>
                string Generator { get; set; }

                /// <summary>
                ///     Gets or sets the image.
                /// </summary>
                IImage Image { get; set; }

                /// <summary>
                ///     Gets or sets the item.
                /// </summary>
                IEnumerable <IItem> Item { get; set; }

                /// <summary>
                ///     Gets or sets the language.
                /// </summary>
                /// <remarks>should comply with values defined by W3C</remarks>
                string Language { get; set; }

                /// <summary>
                ///     Gets or sets the last build date.
                /// </summary>
                /// <remarks>Last time the contents changed</remarks>
                DateTime LastBuildDate { get; set; }

                /// <summary>
                ///     Gets or sets the link.
                /// </summary>
                /// <remarks>The URL to the HTML website corresponding to the channel</remarks>
                Uri Link { get; set; }

                /// <summary>
                ///     Gets or sets the managing editor.
                /// </summary>
                /// <remarks>email address</remarks>
                string ManagingEditor { get; set; }

                /// <summary>
                ///     Gets or sets the pub date.
                /// </summary>
                /// <remarks>
                ///     This should be serialised in a specific format RFC 822
                ///     This is the publication date e.g for a daily newspaper it would change every 24 hours
                /// </remarks>
                DateTime PubDate { get; set; }

                /// <summary>
                ///     Gets or sets the rating.
                /// </summary>
                string Rating { get; set; }

                /// <summary>
                ///     Gets or sets the skip days.
                /// </summary>
                /// <remarks>aggregator hint</remarks>
                IEnumerable <string> SkipDays { get; set; }

                ///// <summary>
                ///// Gets or sets the text input.
                ///// </summary>
                ///// <remarks>rarely used</remarks>
                //ITextInput TextInput { get; set; }

                /// <summary>
                ///     Gets or sets the skip hours.
                /// </summary>
                /// <remarks>aggregator hint</remarks>
                IEnumerable <int> SkipHours { get; set; }

                /// <summary>
                ///     Gets or sets the title.
                /// </summary>
                /// <remarks>
                ///     The name of the channel, this is how people refer to your service
                ///     Generally it will be the same as the title of the website
                /// </remarks>
                string Title { get; set; }

                /// <summary>
                ///     Gets or sets the ttl.
                /// </summary>
                /// <remarks> How long the channel can be cached before refreshing from source</remarks>
                int? Ttl { get; set; }

                /// <summary>
                ///     Gets or sets the web master.
                /// </summary>
                /// <remarks>email address</remarks>
                string WebMaster { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }