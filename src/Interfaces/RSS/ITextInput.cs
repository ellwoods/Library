﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=ITextInput.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:37 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The TextInput interface.
        /// </summary>
        public interface ITextInput
            {
                /// <summary>
                ///     Gets or sets the description.
                /// </summary>
                string Description { get; set; }

                /// <summary>
                ///     Gets or sets the link.
                /// </summary>
                Uri Link { get; set; }

                /// <summary>
                ///     Gets or sets the name.
                /// </summary>
                string Name { get; set; }

                /// <summary>
                ///     Gets or sets the title.
                /// </summary>
                string Title { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }