﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IFeed.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 13/07/2017 10:36 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace Library.Interfaces.RSS
    {
        /// <summary>
        ///     The Feed interface.
        /// </summary>
        public interface IFeed
            {
                /// <summary>
                ///     Gets or sets the channel.
                /// </summary>
                IChannel Channel { get; set; }

                /// <summary>
                ///     Gets or sets the version.
                /// </summary>
                /// <remarks>Thsi is the RSS version, generally this will be 2.0</remarks>
                string Version { get; set; }

                /// <summary>
                ///     The serializer.
                /// </summary>
                /// <returns>
                ///     A <see cref="string" /> serialisation of this object.
                /// </returns>
                string Serialize();
            }
    }