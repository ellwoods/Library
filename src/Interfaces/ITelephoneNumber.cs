//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=ITelephoneNumber.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 13:01 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using Library.Enums;

namespace Library.Interfaces
    {
        /// <summary>
        ///     The TelephoneNumber interface.
        /// </summary>
        public interface ITelephoneNumber : IEquatable<ITelephoneNumber>
            {
                /// <summary>
                ///     Telephone Number Value
                /// </summary>
                string Value { get; set; }

                /// <summary>
                ///     Country Code
                /// </summary>
                /// <value>string</value>
                /// <returns>The country code for the number</returns>
                /// <remarks>+44 for UK</remarks>
                string CountryCode { get; set; }

                /// <summary>
                ///     Gets or sets the extension number.
                /// </summary>
                string ExtensionNumber { get; set; }

                /// <summary>
                ///     Mobile
                /// </summary>
                /// <value>boolean</value>
                /// <returns>true if the phone is a mobile, false otherwise</returns>
                /// <remarks></remarks>
                bool Mobile { get; set; }

                /// <summary>
                ///     Full National number
                /// </summary>
                /// <value>string</value>
                /// <returns>The full national telephone number</returns>
                /// <remarks>This should be the full national number i.e. with no leading 0</remarks>
                string NationalNumber { get; set; }

                /// <summary>
                ///     Preferred
                /// </summary>
                /// <value>boolean</value>
                /// <returns>True if the number is the preferred number, false otherwise</returns>
                /// <remarks></remarks>
                bool Preferred { get; set; }

                /// <summary>
                ///     Gets or sets the telephone use.
                /// </summary>
                TelephoneUseType TelephoneUse { get; set; }
            }
    }