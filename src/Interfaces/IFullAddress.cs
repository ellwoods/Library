﻿namespace Library.Interfaces
    {
        /// <summary>
        ///     Full Address details
        /// </summary>
        /// <remarks>
        ///     This includes address lines, uprn, usrn etc.
        /// </remarks>
        public interface IFullAddress :IAddress, ISimpleAddress
            {
            }
    }