//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=FullContact.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 06/07/2020 10:01
//  Altered - 06/07/2020 12:40 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using Library.Interfaces;

namespace Library
    {
        /// <summary>
        ///     Full Contact
        /// </summary>
        /// <remarks>
        ///     Includes full address details
        /// </remarks>
        public class FullContact : IFullContact
            {
                public FullContact() => Setup(new FullAddressDetail());

                public FullContact(IFullAddress address) => Setup(address);

                private void Setup(IFullAddress address)
                    {
                        Person = new SimplePerson();
                        Address = address;
                        PreferredPhone = new SimpleTelephoneNumber();
                        HomePhone = new SimpleTelephoneNumber();
                        MobilePhone = new SimpleTelephoneNumber();
                        WorkPhone = new SimpleTelephoneNumber();
                    }

                #region Implementation of IFullContact

                /// <inheritdoc />
                public IPerson Person { get; set; }

                /// <inheritdoc />
                public IFullAddress Address { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber PreferredPhone { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber HomePhone { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber MobilePhone { get; set; }

                /// <inheritdoc />
                public ITelephoneNumber WorkPhone { get; set; }

                /// <inheritdoc />
                public string emailAddress { get; set; }

                /// <inheritdoc />
                public string Notes { get; set; }

                #endregion
            }
    }