//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=IPostCodeValidator.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 06/07/2020 12:58
//  Altered - 06/07/2020 13:02 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace Library.Validators
    {
        /// <summary>
        /// </summary>
        public interface IPostCodeValidator
            {
                /// <summary>
                /// </summary>
                /// <param name="postCode"></param>
                /// <returns></returns>
                bool ValidPostCode(string postCode);
            }
    }