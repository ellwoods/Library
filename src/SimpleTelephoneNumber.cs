//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=SimpleTelephoneNumber.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 12:50 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using Library.Enums;
using Library.Interfaces;

namespace Library
    {
        /// <summary>
        ///     Simplified phone number
        /// </summary>
        /// <remarks>This is for simple cases where basically just a number is needed</remarks>
        public sealed class SimpleTelephoneNumber : ITelephoneNumber, IEquatable<SimpleTelephoneNumber>
            {
                public SimpleTelephoneNumber() => Value = "";

                public SimpleTelephoneNumber(string phoneNumber) => Value = phoneNumber;

                /// <summary>
                /// </summary>
                public string Number => CountryCode.Trim() + Value.Trim();

                /// <summary>
                /// </summary>
                public string Value { get; set; }


                /// <inheritdoc />
                public string CountryCode { get; set; } = "";

                /// <inheritdoc />
                public string ExtensionNumber { get; set; } = "";

                /// <inheritdoc />
                public bool Mobile { get; set; }

                /// <inheritdoc />
                public string NationalNumber
                {
                    get => Number;
                    set => this.Value = Value;
                }

                /// <inheritdoc />
                public bool Preferred { get; set; }

                /// <inheritdoc />
                public TelephoneUseType TelephoneUse { get; set; }


                /// <inheritdoc />
                public bool Equals(ITelephoneNumber other) => Equals(other as SimpleTelephoneNumber);

                /// <summary>
                /// </summary>
                /// <param name="phone"></param>
                /// <returns></returns>
                public override bool Equals(object phone) => Equals(phone as SimpleTelephoneNumber);

                /// <summary>
                /// </summary>
                /// <param name="other"></param>
                /// <returns></returns>
                public bool Equals(SimpleTelephoneNumber other)
                    {
                        // If parameter is null, return false.
                        if (other is null)
                            {
                                return false;
                            }

                        // Optimization for a common success case.
                        if (ReferenceEquals(this, other))
                            {
                                return true;
                            }

                        // If run-time types are not exactly the same, return false.
                        if (GetType() != other.GetType())
                            {
                                return false;
                            }

                        // Return true if the fields match.
                        // Note that the base class is not invoked because it is
                        // System.Object, which defines Equals as reference equality.
                        return Value == other.Value;
                    }

                /// <summary>
                /// </summary>
                /// <returns></returns>
                public override int GetHashCode()
                    {
                        // from https://www.loganfranken.com/blog/692/overriding-equals-in-c-part-2/
                        // derived from http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
                        unchecked
                            {
                                // Choose large primes to avoid hashing collisions
                                const int hashingBase = (int)2166136261;
                                const int hashingMultiplier = 16777619;

                                var hash = hashingBase;
                                hash = (hash * hashingMultiplier) ^ (Value is object ? Value.GetHashCode() : 0);
                                return hash;
                            }
                    }


                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator ==(SimpleTelephoneNumber lhs, SimpleTelephoneNumber rhs)
                    {
                        // Check for null on left side.
                        if (lhs is null)
                            {
                                return rhs is null;

                                // Only the left side is null.
                            }

                        // Equals handles case of null on right side.
                        return lhs.Equals(rhs);
                    }

                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator !=(SimpleTelephoneNumber lhs, SimpleTelephoneNumber rhs) => !(lhs == rhs);
            }
    }