//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=TelephoneNumber.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 06/07/2020 10:01
//  Altered - 06/07/2020 12:50 - Stephen Ellwood
// 
//  Project : - Library
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Library.Enums;
using Library.Interfaces;

namespace Library
    {
        // this encapsulates the telephone number as defined in 
        // http://umbr4.cabinetoffice.gov.uk/govtalk/schemasstandards/e-gif/datastandards/contact_information/uk_telephone_number.aspx

        /// <summary>
        ///     Telephone number
        /// </summary>
        /// <remarks>Ensure the phone number is correctly formatted </remarks>
        public sealed class TelephoneNumber : ITelephoneNumber, IEquatable<TelephoneNumber>
            {
                /// <summary>
                ///     Initializes a new instance of the <see cref="TelephoneNumber" /> class.
                /// </summary>
                public TelephoneNumber()
                    {
                    }

                /// <summary>
                ///     Initializes a new instance of the <see cref="TelephoneNumber" /> class.
                /// </summary>
                /// <param name="telephoneNumber">The telephone number.</param>
                public TelephoneNumber(string telephoneNumber) =>
                    NationalNumber = telephoneNumber == null ? string.Empty : telephoneNumber.Trim();

                /// <summary>
                ///     Value of phone number
                /// </summary>
                public string Value
                {
                    get => NationalNumber;
                    set => NationalNumber = value;
                }

                /// <summary>
                ///     Country Code
                /// </summary>
                /// <value>string</value>
                /// <returns>The country code for the number</returns>
                /// <remarks>+44 for UK</remarks>
                public string CountryCode { get; set; }

                /// <summary>
                ///     Gets or sets the extension number.
                /// </summary>
                /// <value>
                ///     The extension number.
                /// </value>
                public string ExtensionNumber { get; set; } = "";

                /// <summary>
                ///     Mobile
                /// </summary>
                /// <value>boolean</value>
                /// <returns>true if the phone is a mobile, false otherwise</returns>
                /// <remarks></remarks>
                public bool Mobile { get; set; }

                /// <summary>
                ///     Full National number
                /// </summary>
                /// <value>string</value>
                /// <returns>The full national telephone number</returns>
                /// <remarks>This should be the full national number i.e. with no leading 0</remarks>
                public string NationalNumber { get; set; }

                /// <summary>
                ///     Preferred
                /// </summary>
                /// <value>boolean</value>
                /// <returns>True if the number if the preferred number, false otherwise</returns>
                /// <remarks></remarks>
                public bool Preferred { get; set; }

                /// <summary>
                ///     Gets or sets the telephone use.
                /// </summary>
                /// <value>
                ///     The telephone use.
                /// </value>
                public TelephoneUseType TelephoneUse { get; set; }


                /// <inheritdoc />
                public bool Equals(ITelephoneNumber other) => Equals(other as TelephoneNumber);

                /// <summary>
                /// </summary>
                /// <param name="other"></param>
                /// <returns></returns>
                public bool Equals(TelephoneNumber other)
                    {
                        // If parameter is null, return false.
                        if (other is null)
                            {
                                return false;
                            }

                        // Optimization for a common success case.
                        if (ReferenceEquals(this, other))
                            {
                                return true;
                            }

                        // If run-time types are not exactly the same, return false.
                        if (GetType() != other.GetType())
                            {
                                return false;
                            }

                        // Return true if the fields match.
                        // Note that the base class is not invoked because it is
                        // System.Object, which defines Equals as reference equality.
                        return Value == other.Value;
                    }


                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator ==(TelephoneNumber lhs, TelephoneNumber rhs)
                    {
                        // Check for null on left side.
                        if (lhs is null)
                            {
                                return rhs is null;

                                // Only the left side is null.
                            }

                        // Equals handles case of null on right side.
                        return lhs.Equals(rhs);
                    }

                /// <summary>
                /// </summary>
                /// <param name="lhs"></param>
                /// <param name="rhs"></param>
                /// <returns></returns>
                public static bool operator !=(TelephoneNumber lhs, TelephoneNumber rhs) => !(lhs == rhs);


                /// <summary>
                /// </summary>
                /// <param name="obj"></param>
                /// <returns></returns>
                public override bool Equals(object obj) => Equals(obj as TelephoneNumber);

                /// <summary>
                /// </summary>
                /// <returns></returns>
                public override int GetHashCode()
                    {
                        var hashCode = -2017556967;
                        hashCode = (hashCode * -1521134295) + base.GetHashCode();
                        hashCode = (hashCode * -1521134295) +
                                   EqualityComparer<string>.Default.GetHashCode(NationalNumber);
                        hashCode = (hashCode * -1521134295) +
                                   EqualityComparer<string>.Default.GetHashCode(ExtensionNumber);
                        return hashCode;
                    }
            }
    }