//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=FileInfoExtensions.cs company="North Lincolnshire Council">
//  Solution : -  NLBC.Library  
//  
//  </copyright>
//  <summary>
// 
//  Created - 04/01/2017 15:13
//  Altered - 25/04/2018 11:03 - Stephen Ellwood
//  
//  Project : - NLBC.Library
//   
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

namespace Library.Extensions
    {
        /// <summary> FileInfo object extension </summary>
        /// <remarks>Extensions to the inbuilt FileInfo object.</remarks>
        public static class FileInfoExtensions
            {
  

                ///// <summary> File Owner </summary>
                ///// <param name="path">Path to file of interest</param>
                ///// <returns>string containing the NT owner of the file</returns>
                ///// <remarks>Attempts to calculate who "owns" the file</remarks>
                //public static string Owner(this string path)
                //    {
                //        var f = new FileInfo(path);

                //        // now use the function below
                //        return Owner(f);
                //    }

                ///// <summary> File Owner </summary>
                ///// <param name="file">file of interest</param>
                ///// <returns>string containing the NT owner of the file, "Indeterminate" if an exception occurs</returns>
                ///// <remarks>Attempts to calculate who "owns" the file</remarks>
                //public static string Owner(this FileInfo file)
                //    {
                //        // to start this process we need a fileinfo object
                //        try
                //            {
                //                // now we have the fileinfo we can get the filesecurity object

                //                var a = file.GetAccessControl(AccessControlSections.Owner);

                //                var ownr = a.GetOwner(typeof(NTAccount));

                //                return ownr.Value;
                //            }
                //        catch (Exception)
                //            {
                //                return "Indeterminate";
                //            }
                //    }

                ///// <summary> FileInfo size </summary>
                ///// <param name="file">The FileInfo of interest</param>
                ///// <param name="scale">Optional parameter that indicates what scale we want for the output</param>
                ///// <returns>String with the size and scale e.g. 35 bytes, 2.3 Gb etc.</returns>
                ///// <remarks>
                /////     <seealso cref="SimpleDirList.Size"></seealso>
                ///// </remarks>
                //public static string Size(this FileInfo file,
                //    DirectorySize scale = DirectorySize.Bytes)
                //    {
                //        var d = new SimpleDirList();

                //        return d.FileSize(file,
                //                          scale);
                //    }

                ///// <summary> FileInfo size </summary>
                ///// <param name="path">The Path to the file of interest</param>
                ///// <param name="scale">Optional parameter that indicates what scale we want for the output</param>
                ///// <returns>String with the size and scale e.g. 35 bytes, 2.3 Gb etc.</returns>
                ///// <remarks>
                /////     <seealso cref="SimpleDirList.Size"></seealso>
                ///// </remarks>
                //public static string Size(this string path,
                //    DirectorySize scale = DirectorySize.Bytes)
                //    {
                //        var d = new SimpleDirList();

                //        return d.FileSize(path,
                //                          scale);
                //    }
            }
    }