//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=TelephoneNumberTests.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 12:56 - Stephen Ellwood
// 
//  Project : - Library.tests
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using Library.Enums;
using Library.Interfaces;
using NUnit.Framework;

namespace Library.tests
    {
        [TestFixture]
        public class TelephoneNumberTests
            {
                [SetUp]
                public void Setup() => _sut = new TelephoneNumber();

                private TelephoneNumber _sut;


                [Test]
                public void ClassIsNotNull()
                    {
                        _sut.CountryCode = "+44";
                        _sut.ExtensionNumber = "";
                        _sut.Mobile = false;
                        _sut.Preferred = true;
                        _sut.TelephoneUse = TelephoneUseType.Home;

                        Assert.That(_sut, Is.Not.NaN);
                        Assert.That(_sut.Value, Is.Null);
                        Assert.That(_sut, Is.InstanceOf<ITelephoneNumber>());
                    }

                [Test]
                public void EmptyInputReturnsEmptyPhoneNumber()
                    {
                        const string input = "";

                        var actual = new TelephoneNumber(input);

                        Assert.That(actual, Is.Not.Null);
                        Assert.That(actual.Value, Is.EqualTo(input));
                    }

                [Test]
                public void NullInputReturnsEmptyPhoneNumber()
                    {
                        string input = null;

                        var actual = new TelephoneNumber(input);

                        Assert.That(actual, Is.Not.Null);
                        Assert.That(actual.Value, Is.EqualTo(""));
                    }

                [Test]
                public void SimpleNumberReturnsExpected()
                    {
                        var number = "12345 5789";

                        var simpleTel = new TelephoneNumber {Value = number};

                        var actual = simpleTel.Value;

                        Assert.That(actual, Is.EqualTo(number));
                    }

                [Test]
                [Sequential]
                public void TelephoneNumberIsEqual(
                    [Values("1", "2", "3")] string x,
                    [Values("1", "2", "3")] string s)
                    {
                        var numTelephoneNumber = new TelephoneNumber(x);

                        var stringTelephoneNumber = new TelephoneNumber(s);

                        var actual = numTelephoneNumber == stringTelephoneNumber;

                        Assert.That(actual, Is.True);

                        Assert.That(numTelephoneNumber, Is.EqualTo(stringTelephoneNumber));
                    }

                [Test]
                public void TelephoneNumberIsEquatable()
                    {
                        var TelephoneNumber = new TelephoneNumber();

                        Assert.IsInstanceOf<IEquatable<ITelephoneNumber>>(TelephoneNumber);
                    }

                [Test]
                [Combinatorial]
                public void TelephoneNumberIsInequal(
                    [Values("1", "2", "3")] string x,
                    [Values("", "12")] string s)
                    {
                        var numTelephoneNumber = new TelephoneNumber(x);

                        var stringTelephoneNumber = new TelephoneNumber(s);

                        Assert.That(numTelephoneNumber, Is.Not.EqualTo(stringTelephoneNumber));

                        Assert.That(!numTelephoneNumber.Equals(stringTelephoneNumber));

                        Assert.That(numTelephoneNumber != stringTelephoneNumber);
                    }

                [Test]
                [Sequential]
                public void TelephoneNumberIsInequalTo(
                    [Values("1", "2", "3")] string x,
                    [Values("1", "2", "3")] string s)
                    {
                        var numTelephoneNumber = new TelephoneNumber(x);

                        var stringTelephoneNumber = new TelephoneNumber(s);

                        var actual = numTelephoneNumber != stringTelephoneNumber;

                        Assert.That(actual, Is.False);
                    }

                [Test]
                [Combinatorial]
                public void TelephoneNumberIsNotEqual(
                    [Values("1", "2", "3")] string x,
                    [Values("", "12")] string s)
                    {
                        var numTelephoneNumber = new TelephoneNumber(x);

                        var stringTelephoneNumber = new TelephoneNumber(s);

                        var actual = numTelephoneNumber == stringTelephoneNumber;

                        Assert.That(actual, Is.False);

                        Assert.That(numTelephoneNumber, Is.Not.EqualTo(stringTelephoneNumber));
                    }
            }
    }