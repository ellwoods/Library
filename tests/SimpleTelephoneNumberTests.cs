//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file=SimpleTelephoneNumberTests.cs company="North Lincolnshire Council">
//  Solution : -  Library
// 
//  </copyright>
//  <summary>
// 
//  Created - 03/07/2020 17:11
//  Altered - 06/07/2020 12:56 - Stephen Ellwood
// 
//  Project : - Library.tests
// 
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using System;
using Library.Interfaces;
using NUnit.Framework;

namespace Library.tests
    {
        [TestFixture]
        [Category("LIBRARY")]
        [Category("SimpleTelephoneNumber")]
        public class SimpleTelephoneNumberTests
            {
                [SetUp]
                public void Setup() => _sut = new SimpleTelephoneNumber();

                private SimpleTelephoneNumber _sut;


                [Test]
                public void ClassIsNotNull()
                    {
                        Assert.That(_sut, Is.Not.NaN);
                        Assert.That(_sut.Value, Is.EqualTo(""));
                        Assert.That(_sut, Is.InstanceOf<ITelephoneNumber>());
                    }

                [Test]
                public void EmptyInputReturnsEmptyPhoneNumber()
                    {
                        const string input = "";

                        var actual = new TelephoneNumber(input);

                        Assert.That(actual, Is.Not.Null);
                        Assert.That(actual.Value, Is.EqualTo(input));
                    }

                [Test]
                public void NullInputReturnsEmptyPhoneNumber()
                    {
                        string input = null;

                        var actual = new TelephoneNumber(input);

                        Assert.That(actual, Is.Not.Null);
                        Assert.That(actual.Value, Is.EqualTo(""));
                    }

                [Test]
                public void SimpleNumberNationalNumberReturnsExpected()
                    {
                        var code = "123";
                        var number = "12345 5789";

                        var expected = code + number;

                        var simpleTel = new SimpleTelephoneNumber(number) {CountryCode = code};

                        var actual = simpleTel.NationalNumber;

                        Assert.That(actual, Is.EqualTo(expected));
                    }

                [Test]
                public void SimpleNumberPlusCodeReturnsExpected()
                    {
                        var code = "123";
                        var number = "12345 5789";

                        var expected = code + number;

                        var simpleTel = new SimpleTelephoneNumber(number) {CountryCode = code};

                        var actual = simpleTel.Number;

                        Assert.That(actual, Is.EqualTo(expected));
                    }

                [Test]
                public void SimpleNumberReturnsExpected()
                    {
                        var number = "12345 5789";

                        var simpleTel = new SimpleTelephoneNumber(number);

                        var actual = simpleTel.Number;

                        Assert.That(actual, Is.EqualTo(number));
                    }

                [Test]
                [Sequential]
                public void SimpleTelephoneNumberIsEqual(
                    [Values("1", "2", "3")] string x,
                    [Values("1", "2", "3")] string s)
                    {
                        var numSimpleTelephoneNumber = new SimpleTelephoneNumber(x);

                        var stringSimpleTelephoneNumber = new SimpleTelephoneNumber(s);

                        var actual = numSimpleTelephoneNumber == stringSimpleTelephoneNumber;

                        Assert.That(actual, Is.True);

                        Assert.That(numSimpleTelephoneNumber, Is.EqualTo(stringSimpleTelephoneNumber));
                    }

                [Test]
                public void SimpleTelephoneNumberIsEquatable()
                    {
                        var SimpleTelephoneNumber = new SimpleTelephoneNumber();

                        Assert.IsInstanceOf<IEquatable<ITelephoneNumber>>(SimpleTelephoneNumber);
                    }

                [Test]
                [Combinatorial]
                public void SimpleTelephoneNumberIsInequal(
                    [Values("1", "2", "3")] string x,
                    [Values("", "12")] string s)
                    {
                        var numSimpleTelephoneNumber = new SimpleTelephoneNumber(x);

                        var stringSimpleTelephoneNumber = new SimpleTelephoneNumber(s);

                        var actual = numSimpleTelephoneNumber != stringSimpleTelephoneNumber;

                        Assert.That(actual, Is.True);
                    }

                [Test]
                [Sequential]
                public void SimpleTelephoneNumberIsInequalTo(
                    [Values("1", "2", "3")] string x,
                    [Values("1", "2", "3")] string s)
                    {
                        var numSimpleTelephoneNumber = new SimpleTelephoneNumber(x);

                        var stringSimpleTelephoneNumber = new SimpleTelephoneNumber(s);

                        var actual = numSimpleTelephoneNumber != stringSimpleTelephoneNumber;

                        Assert.That(actual, Is.False);
                    }

                [Test]
                [Combinatorial]
                public void SimpleTelephoneNumberIsNotEqual(
                    [Values("1", "2", "3")] string x,
                    [Values("", "12")] string s)
                    {
                        var numSimpleTelephoneNumber = new SimpleTelephoneNumber(x);

                        var stringSimpleTelephoneNumber = new SimpleTelephoneNumber(s);

                        var actual = numSimpleTelephoneNumber == stringSimpleTelephoneNumber;

                        Assert.That(actual, Is.False);

                        Assert.That(numSimpleTelephoneNumber, Is.Not.EqualTo(stringSimpleTelephoneNumber));
                    }
            }
    }