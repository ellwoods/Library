![NLBC Logo](nlbc.png)

![.NET Core](https://github.com/NelNlc/Library/workflows/.NET%20Core/badge.svg) 
[![Build Status](https://dev.azure.com/NLBC/Library/_apis/build/status/NelNlc.Library?branchName=master)](https://dev.azure.com/NLBC/Library/_build/latest?definitionId=3&branchName=master)
[![Build Status](https://travis-ci.org/NelNlc/Library.svg?branch=master)](https://travis-ci.org/NelNlc/Library)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/a046e6478b144bd08c6d00bb623c95b8)](https://www.codacy.com/gh/NelNlc/Library?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=NelNlc/Library&amp;utm_campaign=Badge_Grade)

# Code Library

Library of extensions and models for netstandard 2.0. 
This is the latest incarnation of this library which has been through a number of iterations in a variety of languages. 
Some of the functionality is now available in standard libraries and can be ignored, it has been left in for legacy reasons in particular where there are slight differences in functionality.

## Extensions


### Strings

This is a range of string operations, including a concept of words ie a set of characters with a given delimiter

-   Boolean Value - attempts to determine if a string contains some sort of boolean e.g. "true", "no", "0"
-   Instr - the C# equivalent of the VB function
-   Replace
-   Right - equivalent to the VB function

#### String tests

-   Is Numeric - attempts to determine if a string contains a valid Number
-   Is Numeric Characters - checks for numeric only characters in a String
-   Is Long
-   Is Integer
-   Is Alphabetic - checks for alphabetic only characters - a-z
-   Is Alphabetic character
-   Is Numeric character
-   Is Alphanumeric - checks a string only contains alphanumerics
-   Is Alphanumeric character
-   Is Valid file name - checks that the characters in the file name are valid for Windows
-   Is Unicode
-   Is All Whitespace - checks if all of a string is whitespace

#### Word functions

-   Word Count
-   first Word
-   Nth Word
-   Last Word
-   Truncate - truncates a string so that words aren't chopped off mid Word. Optionally adds an ellipsis too
-   split - returns a string array of the words in the input
-   Camel case split

#### Replace functions

-   Replace All - replaces all instances of a substring in the input
-   Replace All From - Replaces all instances of a substring from the input starting at a specific point in the input
-   Replace To 
-   Replace All To - Replaces all instances of a substring from the input up to a given location
-   Replace Mid - replaces a substring within a substring of the input
-   ReplaceAllMid - replaces all substrings within a substring of the input
-   Replace Whitespace
-   Replace Unicode
-   Remove Unicode
-   LTrim, RTrim
-   Replace Duplicate spaces
-   Sanitise - returns the input with all of the set of characters provided having been removed
-   Sanitise Non Alpha Numerics - attempts to remove all non alphanumerics from the input (optionally keeps spaces, optionally keeps simple punctuation e.g. full stops)

#### string formatting

-   Pretty XML - Takes an XML string and outputs it *prettified*

### DateTime

This includes date calculations and checks. Unless otherwise specified these are based on UK format dates

-   Easter date
-   First Monday of month date
-   ISO Week
-   Last day of month 
-   Last Monday of month date
-   Next Monday date
-   RSSFormat - Format date as RSS style DateTime
-   Between - Determines if a date is between two others
-   Previous 1st April date
-   Is UK English bank holiday
-   Is English bank holiday week - is there an english/welsh bank holiday in this week (starting on the Monday)
-   Is UK English working day - this checks if the day is a weekend or an english/welsh bank holiday
-   Daylight Savings Time UK end - Date that DST ends in the UK
-   Daylight Savings Time UK start - Start of DST in the UK
-   Is in daylight savings time UK
-   Is Date
-   Is Any Date - checks if date, including Unix style dates

### Enumerable

 -   Pick Random
 -   Shuffle
 -   Split To list - split into fixed size pages, possibly with some remainder in the final page
 -   Split to buckets - splits an enumerable into sublists of equal size with the remainder being distributed equally

### Uri

 -   Is Bookmark
 -   Relative to absolute
 -   Is page bookmark - is a link to a bookmark on the same page
 -   Root
 -   Fix path - adds trailing slash if missing
 -   Depth
 -   Remove bookmark element - remove bookmark from link
 -   Page base - root of page, removes page element from link
 -   Is mail - is the link a mailto
 -   Has Query
 -   Is same root as - are two domains the same treating http and https as the same
 -   Is same page root as - are the two page roots the same treating http and https as the same

### NLC specific

Two methods used for validity checks at North Lincolnshire Council

-   Is Valid CRM Number
-   Is Valid Text - this checks that only allowed characters are in the input Text

### e-Gif

This implements some of the business rules from the former data standards catalog. 

There are validity checks for 

-   National Insurance Number
-   NASS number
-   Dates - in governmental specified format (CCYY-MM-DD)
-   UPRN
-   USRN
-   UK PostCode - note however that this may be rendered partially redundant by changes made by the post office


## Models

These classes are for general use. Many of them are fairly trivial however they have validation and are useful for overloading functions for what are essentially the same datatypes

 -   Post Code
 -   Uprn
 -   Usrn
 -   SimplePerson - basic representation of an individual, primarily name
 -   SimpleAddress - basic address with named fields
 -   SimpleAddressLine - basic address in the form of address lines
 -   SimpleAddressNameNumber - basic address with house name and house number fields 
 -   FullAddressDetail - Full address detail includes uprn, usrn
 -   SimpleContact - basic representation of contact details, including address, phone numbers etc
 -   FullContact - Similar to SimpleContact but contains full address
 -   SimpleTelephoneNumber
 -   CRM Reference - this is NLC specific but could be adapted
 -   NASS 
 -   National Insurance number (NINO)

NOTE - the address classes have an equality operator, including between the different types. 
Person has no equality operator as that could be taken to imply two people are the same when they aren't
e.g. S. Ellwood could be Sam or Sophie

&copy; SE 2020
